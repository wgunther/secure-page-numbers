\documentclass{beamer}

\mode<presentation>
{
  \usetheme{Frankfurt}
}

\setbeamertemplate{navigation symbols}{}%remove navigation symbols

\usepackage[english]{babel}
% or whatever

\usepackage{times}
\usepackage[T1]{fontenc}
% Or whatever. Note that the encoding and the font should match. If T1
% does not look nice, try deleting the line with the fontenc.

\usepackage{graphicx}

\usepackage{pgfplots}

\newdimen\xsize

\def\redact#1{\leavevmode
	\setbox0\hbox{X}%
	\xsize=\wd0%
	\hbox{\box0\hskip-\xsize\hbox to \xsize{\hss#1\hss}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{Cryptographically secure page numbering in~\LaTeX}

\author{William Gunther and Brian Kell\footnote{Respectively.}}

\institute{{\normalsize Epic and Google, Inc.\footnote{Respectively.}}}

\date{SIGBOVIK '16 \\ April~1, 2016}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

%\begin{frame}{Outline}
%  \tableofcontents
%  % You might wish to add the option [pausesections]
%\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\redact1. Introduction}

\begin{frame} \frametitle{\redact1. Introduction}
	\begin{itemize}
		\item Recent advances in cryptography have been applied to online
			communication, commerce, information storage hardware, voice
			correspondence, etc.
		\item Surprisingly, strong cryptographic techniques have not yet been
			applied to page numbers.
		\item Page numbers are ubiquitous and quite important for page-based
			communication.
		\item Our work makes the first steps toward bringing modern
			cryptography into the 1470s by introducing a \LaTeX\ macro package
			to enable cryptographically secure page numbers.
			\vskip-0.05in
			\begin{itemize}
				\item Ideally we would also secure section numbers, figure
					numbers, citation numbers, etc. As a provisional security
					measure, our package provides facilities to securely
					redact these numbers.
			\end{itemize}
	\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\redact2. Motivation}

\begin{frame} \frametitle{\redact2. Motivation}
	\begin{enumerate}
		\item Scenario: Alice is reading a sensitive document.
		\pause
		\item Eve glances over Alice's shoulder and reads unencrypted page
			number.
		\pause
		\item If Eve can also get the last page number, she can quickly compute
			the percentage of document that has been read.
		\pause
		\item Two such observations, timed carefully with stopwatch or large
			sundial, provide Eve with enough information to estimate remaining
			reading time.
		\pause
		\item Eve can commence other attacks, knowing that Alice will be busy
			reading the sensitive document.
	\end{enumerate}
\end{frame}

\begin{frame} \frametitle{\redact2.\redact1. Previous work}
	\begin{itemize}
		\item An early technique for page-number encryption (and most commonly
			employed today) is a Roman cryptosystem for page numbers in
			sensitive early sections of a document, such as prefaces,
			forewords, and tables of contents.
			\begin{itemize}
				\item Unfortunately, this technique has never been supported by
					strong cryptographic justification.
				\item Recent work has identified significant flaws: Hagenfried
					et~al.\ describe an attack that recovered plaintext~``24''
					from encrypted page number ``xxiv'' in less than 7~hours on
					commodity hardware.
			\end{itemize}
	\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\redact3. Secure page numbers in \LaTeX}

\begin{frame}[fragile] \frametitle{\redact3. Secure page numbers in \LaTeX}
	\begin{itemize}
		\item We have written a \LaTeX\ macro package to enable
			cryptographically secure page numbering.
		\item In the preamble of any \LaTeX\ document:\\
			\verb|\usepackage{secure-page-numbers}|
		\item To cryptographically hash page numbers:\\
			\verb|\pagenumbering{shahash}|
		\item To securely redact section numbers:\\
			\verb|\redactsectionnumbers|
		\item For full usage instructions, see the paper.
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{\redact3.\redact1. Implementation details}
	\begin{itemize}
		\item Using SHA-256 hash of salted page number.
		\item Attempted to implement SHA-256 entirely in \TeX\ macros, but were
			surprised to find that \TeX\ has no native support for bitwise
			operations on 32-bit unsigned integers!
			\begin{itemize}
				\item We can see no innocent reason to omit such functionality
					from a typesetting system.
				\item We suspect this is an intentional deficiency added by the
					NSA to weaken cryptographic algorithms in \TeX\ documents.
					We urge further investigation into this potential security
					backdoor.
			\end{itemize}
		\item To circumvent this problem, our \LaTeX\ macros call an external
			Perl script to compute hashes.
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Example: Insecure page numbers}
	\vskip-0.1in
	\hbox to\textwidth{\hss
	\includegraphics[height=3.2in]{sample-insecure.pdf}\hss}
\end{frame}

\begin{frame} \frametitle{Example: Secure page numbers}
	\vskip-0.1in
	\hbox to\textwidth{\hss
	\includegraphics[height=3.2in]{sample-secure.pdf}\hss}
\end{frame}

\begin{frame} \frametitle{\redact3.\redact2. Security recommendations}
	\begin{itemize}
		\item When properly used, secure page numbers prevent an adversary from
			determining page order without an impractical $\Omega(n!)$~time
			brute-force attack. To support this security:
			\begin{itemize}
				\item Keep documents as loose pages, preferably one-sided.
				\item Avoid binders, staples, paper clips, file folders, and
					other external devices that may leak information about page
					order.
				\item Best practice: Take all pages of many documents and throw
					them at random into a big sack.
				\item Alternatives: Toss pages down a flight of stairs or off a
					suitably tall cliff.
			\end{itemize}
	\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\redact4. Future work}

\begin{frame} \frametitle{\redact4. Future work}
	\begin{itemize}
		\item Fully encrypting section numbers, figure numbers, citation
			numbers, etc.
		\item RSA encryption for two-way communication.
		\item Automatic shuffling of PDF pages.
			\begin{itemize}
				\item Workaround: Save PDFs on unlabeled floppy disks and
					shuffle them together.
			\end{itemize}
	\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\redact5. Conclusions}

\begin{frame} \frametitle{\redact5. Conclusions}
	\begin{itemize}
		\item Introduced a \LaTeX\ package for cryptographically securing page
			numbers.
		\item We urge adoption as soon as possible. We are pleased to see that
			the SIGBOVIK organizers used secure page numbers for the
			proceedings.
	\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}


