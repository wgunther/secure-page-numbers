#include <stdio.h>

#define TeXif(A) "\\TeXif{\\if %x" #A "}"
#define L "{"
#define R "}"

#define MAX 16

int main() {
	
	for (int i = 0; i < MAX; ++i) {
		printf(L TeXif(#1) L "%%\n", i);
		for (int j = 0; j < MAX; ++j) {
			printf(TeXif(#2) L "%x" R L, j, i & j);
		}
		printf("\\Error");
		for (int j = 0; j < MAX+1; ++j) {
			printf(R);
		}
	}
	printf(L "\\Error");
	for (int j = 0; j < MAX + 1; ++j) {
		printf(R);
	}

}
