\catcode`\@=11

% See Alan Jeffrey, "Lists in TeX's Mouth," TUGboat, vol. 11 (1990), no. 2,
% pp. 237--245.

% Filename: lambda.sty
% Author: Alan Jeffrey
% Last modified: 12 Feb 1990  (license changed to LPPL: 6 Aug 2013)
%
% Copyright 1990-2013 Alan Jeffrey.
% This file is part of the lambda-lists package.
% 
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2003/12/01 or later.
%
% This style provides a pile of lambda-calculus and list-handling
% macros of an incredibly obtuse nature.  Read lambda-lists.tex to find out
% what they all do and how they do it.  This \TeX\ code was formally
% verified.
% Alan Jeffrey, 25 Jan 1990.

\def\Identity#1{#1}

\def\Error%
   {\errmessage{Abandon verification all 
                ye who enter here}}

\def\First#1#2{#1}
\def\Second#1#2{#2}

\def\Compose#1#2#3{#1{#2{#3}}}

\def\Twiddle#1#2#3{#1{#3}{#2}}

\let\True=\First
\let\False=\Second
\let\Not=\Twiddle

\def\And#1#2{#1{#2}\False}
\def\Or#1#2{#1\True{#2}}

\def\Lift#1#2#3#4{#1{#4}{#2}{#3}{#4}}

\def\Lessthan#1#2{\TeXif{\ifnum#1<#2 }}

\def\gobblefalse\else\gobbletrue\fi#1#2%
   {\fi#1}
\def\gobbletrue\fi#1#2%
   {\fi#2}
\def\TeXif#1%
   {#1\gobblefalse\else\gobbletrue\fi}

\def\Nil#1#2{#2}
\def\Cons#1#2#3#4{#3{#1}{#2}}
\def\Stream#1{\Cons{#1}{\Stream{#1}}}
\def\Singleton#1{\Cons{#1}\Nil}

\def\Head#1{#1\First\Error}
\def\Tail#1{#1\Second\Error}

\def\Foldl#1#2#3%
   {#3{\Foldl@{#1}{#2}}{#2}}
\def\Foldl@#1#2#3#4%
   {\Foldl{#1}{#1{#2}{#3}}{#4}}
\def\Foldr#1#2#3%
   {#3{\Foldr@{#1}{#2}}{#2}}
\def\Foldr@#1#2#3#4%
   {#1{#3}{\Foldr{#1}{#2}{#4}}}

\def\Cat#1#2{\Foldr\Cons{#2}{#1}}

\def\Reverse{\Foldl{\Twiddle\Cons}\Nil}

\def\All#1{\Foldr{\Compose\And{#1}}\True}
\def\Some#1{\Foldr{\Compose\Or{#1}}\False}
\def\Isempty{\All{\First\False}}

\def\Filter#1%
   {\Foldr{\Lift{#1}\Cons\Second}\Nil}

\def\Map#1{\Foldr{\Compose\Cons{#1}}\Nil}

\def\Insert#1#2#3%
   {#3{\Insert@{#1}{#2}}{\Singleton{#2}}}
\def\Insert@#1#2#3#4%
   {#1{#2}{#3}%
      {\Cons{#2}{\Cons{#3}{#4}}}%
      {\Cons{#3}{\Insert{#1}{#2}{#4}}}}
\def\Insertsort#1{\Foldr{\Insert{#1}}\Nil}

\def\Unlistize#1{[#1\Unlistize@{}]}
\def\Unlistize@#1{#1\Foldr\Commaize{}}
\def\Commaize#1#2{, #1#2}

\def\Listize[#1]%
   {\Listize@#1,\relax]}
\def\Listize@#1,#2]%
   {\TeXif{\ifx\relax#2}%
        {\Singleton{#1}}%
        {\Cons{#1}{\Listize@#2]}}}

\def\Show#1[#2]%
   {\Unlistize{#1{\Listize[#2]}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\def\Tokenlistize#1{\Tokenlistize@#1\relax]}
\def\Tokenlistize@#1#2]%
	{\TeXif{\ifx\relax#2}%
		{\Singleton{#1}}%
		{\Cons{#1}{\Tokenlistize@#2]}}}

\def\Untokenlistize{\Foldr\Emit{}}
\def\Emit#1#2{#1#2}

% Merge f e xs ys: Merge the two lists xs and ys into one list, applying the
% function f to corresponding elements in the two lists to produce each element
% of the resulting list. The element e is used as a placeholder element if one
% of the input lists runs out before the other.
%
% Example:
%     Merge f 0 [1,2,3] [5,6,7,8,9]
%         = [(f 1 5), (f 2 6), (f 3 7), (f 0 8), (f 0 9)].
%
% Definition:
%           Merge f e xs ys = xs (Merge' f e ys) (Map (f e) ys)
%        Merge' f e ys x xs = ys (Merge'' f e x xs)
%                                (Cons (f x e) (Map (Twiddle f e) xs))
%     Merge'' f e x xs y ys = Cons (f x y) (Merge f e xs ys)
\def\Merge#1#2#3#4{#3{\Merge@{#1}{#2}{#4}}{\Map{#1{#2}}{#4}}}
\def\Merge@#1#2#3#4#5{#3{\Merge@@{#1}{#2}{#4}{#5}}%
                        {\Cons{#1{#4}{#2}}{\Map{\Twiddle{#1}{#2}}{#5}}}}
\def\Merge@@#1#2#3#4#5#6{\Cons{#1{#3}{#5}}{\Merge{#1}{#2}{#4}{#6}}}

% Add two binary digits to produce 0, 1, or 2. (Actually every token other
% than 0 is considered to be 1.)
\def\Bitadd#1#2{\TeXif{\if0#1}{\TeXif{\if0#2}01}{\TeXif{\if0#2}12}}

% A binary number is a list of bits in little-endian order (i.e., with the
% least significant bit at the head).
\def\Binaryzero{\Nil}
\def\Binaryone{\Singleton1}
\def\Binarytwo{\Tokenlistize{01}}
\def\Binarythree{\Tokenlistize{11}}
\def\Binaryfour{\Tokenlistize{001}}
\def\Binaryfive{\Tokenlistize{101}}
\def\Binarysix{\Tokenlistize{011}}
\def\Binaryseven{\Tokenlistize{111}}
\def\Binaryeight{\Tokenlistize{0001}}
\def\Binarynine{\Tokenlistize{1001}}

% Convert a decimal digit to a binary number.
\def\Decimaldigittobinary#1%
	{\TeXif{\if0#1}\Binaryzero
	{\TeXif{\if1#1}\Binaryone
	{\TeXif{\if2#1}\Binarytwo
	{\TeXif{\if3#1}\Binarythree
	{\TeXif{\if4#1}\Binaryfour
	{\TeXif{\if5#1}\Binaryfive
	{\TeXif{\if6#1}\Binarysix
	{\TeXif{\if7#1}\Binaryseven
	{\TeXif{\if8#1}\Binaryeight
	{\TeXif{\if9#1}\Binarynine
	\Error}}}}}}}}}}

% Performs a carry on a "binary" number that might contain some 2's.
%
% Definition:
%              Carry xs = xs Carry' Nil
%           Carry' x xs = (x == 2) ? (Cons 0 (Carrytheone xs)) :
%                                    (Cons x (Carry xs))
%        Carrytheone xs = xs Carrytheone' (Singleton 1)
%     Carrytheone' x xs = (x == 0) ? (Cons 1 (Carry xs)) :
%                         (x == 1) ? (Cons 0 (Carrytheone xs)) :
%                                    (Cons 1 (Carrytheone xs))
\def\Carry#1{#1\Carry@\Nil}
\def\Carry@#1#2{\TeXif{\if2#1}{\Cons0{\Carrytheone{#2}}}%
                              {\Cons{#1}{\Carry{#2}}}}
\def\Carrytheone#1{#1\Carrytheone@{\Singleton1}}
\def\Carrytheone@#1#2{\TeXif{\if0#1}{\Cons1{\Carry{#2}}}%
                     {\TeXif{\if1#1}{\Cons0{\Carrytheone{#2}}}%
                                    {\Cons1{\Carrytheone{#2}}}}}

% Addition of binary numbers.
\def\Binaryadd#1#2{\Carry{\Merge\Bitadd0{#1}{#2}}}

% Multiplication of binary numbers by some small constants.
\def\Binarydouble{\Cons0}
\def\Binaryquadruple{\Compose\Binarydouble\Binarydouble}
\def\Binaryquintuple#1{\Binaryadd{#1}{\Binaryquadruple{#1}}}
\def\Binarydecuple{\Compose\Binarydouble\Binaryquintuple}

% Decimal-to-binary conversion.
\def\Decimaltobinary#1{#1\Decimaltobinary@\Binaryzero}
\def\Decimaltobinary@#1#2{\Binaryadd{\Binarydecuple{\Decimaltobinary{#2}}}%
                                    {\Decimaldigittobinary{#1}}}

% Demonstration.
\def\Mynumber{\Reverse{\Tokenlistize{1234567890}}}
\message{\Untokenlistize{\Reverse{\Decimaltobinary\Mynumber}}}


% Binary And/Xor/Or

\def\SingleBinaryX@r#1#2{\TeXif{\if#1#2}{0}{1}}
\def\SingleBinary@nd#1#2{\TeXif{\if0#1}{0}{\TeXif{\if0#2}{0}{1}}}
\def\SingleBinary@r#1#2{\TeXif{\if1#1}{1}{#2}}
\def\SingleBinaryN@t#1{\TeXif{if1#1}{0}{1}}

\def\BinaryXor{\Merge\SingleBinaryX@r\Error}
\def\BinaryAnd{\Merge\SingleBinary@nd\Error}
\def\BinaryOr{\Merge\SingleBinary@r\Error}
\def\BinaryNot{\Map\SingleBinaryN@t}

% Hex And/Xor/Or

\def\SingleHexN@t#1{%
\TeXif{\if 0#1}{f}{\TeXif{\if 1#1}{e}{\TeXif{\if 2#1}{d}{\TeXif{\if 3#1}{c}{\TeXif{\if 4#1}{b}{\TeXif{\if 5#1}{a}{\TeXif{\if 6#1}{9}{\TeXif{\if 7#1}{8}{\TeXif{\if 8#1}{7}{\TeXif{\if 9#1}{6}{\TeXif{\if a#1}{5}{\TeXif{\if b#1}{4}{\TeXif{\if c#1}{3}{\TeXif{\if d#1}{2}{\TeXif{\if e#1}{1}{\TeXif{\if f#1}{0}{\Error}}}}}}}}}}}}}}}}%
}

\def\SingleHex@r#1#2{%
\TeXif{\if 0#1}{%
\TeXif{\if 0#2}{0}{\TeXif{\if 1#2}{1}{\TeXif{\if 2#2}{2}{\TeXif{\if 3#2}{3}{\TeXif{\if 4#2}{4}{\TeXif{\if 5#2}{5}{\TeXif{\if 6#2}{6}{\TeXif{\if 7#2}{7}{\TeXif{\if 8#2}{8}{\TeXif{\if 9#2}{9}{\TeXif{\if a#2}{a}{\TeXif{\if b#2}{b}{\TeXif{\if c#2}{c}{\TeXif{\if d#2}{d}{\TeXif{\if e#2}{e}{\TeXif{\if f#2}{f}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 1#1}{%
\TeXif{\if 0#2}{1}{\TeXif{\if 1#2}{1}{\TeXif{\if 2#2}{3}{\TeXif{\if 3#2}{3}{\TeXif{\if 4#2}{5}{\TeXif{\if 5#2}{5}{\TeXif{\if 6#2}{7}{\TeXif{\if 7#2}{7}{\TeXif{\if 8#2}{9}{\TeXif{\if 9#2}{9}{\TeXif{\if a#2}{b}{\TeXif{\if b#2}{b}{\TeXif{\if c#2}{d}{\TeXif{\if d#2}{d}{\TeXif{\if e#2}{f}{\TeXif{\if f#2}{f}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 2#1}{%
\TeXif{\if 0#2}{2}{\TeXif{\if 1#2}{3}{\TeXif{\if 2#2}{2}{\TeXif{\if 3#2}{3}{\TeXif{\if 4#2}{6}{\TeXif{\if 5#2}{7}{\TeXif{\if 6#2}{6}{\TeXif{\if 7#2}{7}{\TeXif{\if 8#2}{a}{\TeXif{\if 9#2}{b}{\TeXif{\if a#2}{a}{\TeXif{\if b#2}{b}{\TeXif{\if c#2}{e}{\TeXif{\if d#2}{f}{\TeXif{\if e#2}{e}{\TeXif{\if f#2}{f}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 3#1}{%
\TeXif{\if 0#2}{3}{\TeXif{\if 1#2}{3}{\TeXif{\if 2#2}{3}{\TeXif{\if 3#2}{3}{\TeXif{\if 4#2}{7}{\TeXif{\if 5#2}{7}{\TeXif{\if 6#2}{7}{\TeXif{\if 7#2}{7}{\TeXif{\if 8#2}{b}{\TeXif{\if 9#2}{b}{\TeXif{\if a#2}{b}{\TeXif{\if b#2}{b}{\TeXif{\if c#2}{f}{\TeXif{\if d#2}{f}{\TeXif{\if e#2}{f}{\TeXif{\if f#2}{f}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 4#1}{%
\TeXif{\if 0#2}{4}{\TeXif{\if 1#2}{5}{\TeXif{\if 2#2}{6}{\TeXif{\if 3#2}{7}{\TeXif{\if 4#2}{4}{\TeXif{\if 5#2}{5}{\TeXif{\if 6#2}{6}{\TeXif{\if 7#2}{7}{\TeXif{\if 8#2}{c}{\TeXif{\if 9#2}{d}{\TeXif{\if a#2}{e}{\TeXif{\if b#2}{f}{\TeXif{\if c#2}{c}{\TeXif{\if d#2}{d}{\TeXif{\if e#2}{e}{\TeXif{\if f#2}{f}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 5#1}{%
\TeXif{\if 0#2}{5}{\TeXif{\if 1#2}{5}{\TeXif{\if 2#2}{7}{\TeXif{\if 3#2}{7}{\TeXif{\if 4#2}{5}{\TeXif{\if 5#2}{5}{\TeXif{\if 6#2}{7}{\TeXif{\if 7#2}{7}{\TeXif{\if 8#2}{d}{\TeXif{\if 9#2}{d}{\TeXif{\if a#2}{f}{\TeXif{\if b#2}{f}{\TeXif{\if c#2}{d}{\TeXif{\if d#2}{d}{\TeXif{\if e#2}{f}{\TeXif{\if f#2}{f}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 6#1}{%
\TeXif{\if 0#2}{6}{\TeXif{\if 1#2}{7}{\TeXif{\if 2#2}{6}{\TeXif{\if 3#2}{7}{\TeXif{\if 4#2}{6}{\TeXif{\if 5#2}{7}{\TeXif{\if 6#2}{6}{\TeXif{\if 7#2}{7}{\TeXif{\if 8#2}{e}{\TeXif{\if 9#2}{f}{\TeXif{\if a#2}{e}{\TeXif{\if b#2}{f}{\TeXif{\if c#2}{e}{\TeXif{\if d#2}{f}{\TeXif{\if e#2}{e}{\TeXif{\if f#2}{f}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 7#1}{%
\TeXif{\if 0#2}{7}{\TeXif{\if 1#2}{7}{\TeXif{\if 2#2}{7}{\TeXif{\if 3#2}{7}{\TeXif{\if 4#2}{7}{\TeXif{\if 5#2}{7}{\TeXif{\if 6#2}{7}{\TeXif{\if 7#2}{7}{\TeXif{\if 8#2}{f}{\TeXif{\if 9#2}{f}{\TeXif{\if a#2}{f}{\TeXif{\if b#2}{f}{\TeXif{\if c#2}{f}{\TeXif{\if d#2}{f}{\TeXif{\if e#2}{f}{\TeXif{\if f#2}{f}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 8#1}{%
\TeXif{\if 0#2}{8}{\TeXif{\if 1#2}{9}{\TeXif{\if 2#2}{a}{\TeXif{\if 3#2}{b}{\TeXif{\if 4#2}{c}{\TeXif{\if 5#2}{d}{\TeXif{\if 6#2}{e}{\TeXif{\if 7#2}{f}{\TeXif{\if 8#2}{8}{\TeXif{\if 9#2}{9}{\TeXif{\if a#2}{a}{\TeXif{\if b#2}{b}{\TeXif{\if c#2}{c}{\TeXif{\if d#2}{d}{\TeXif{\if e#2}{e}{\TeXif{\if f#2}{f}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 9#1}{%
\TeXif{\if 0#2}{9}{\TeXif{\if 1#2}{9}{\TeXif{\if 2#2}{b}{\TeXif{\if 3#2}{b}{\TeXif{\if 4#2}{d}{\TeXif{\if 5#2}{d}{\TeXif{\if 6#2}{f}{\TeXif{\if 7#2}{f}{\TeXif{\if 8#2}{9}{\TeXif{\if 9#2}{9}{\TeXif{\if a#2}{b}{\TeXif{\if b#2}{b}{\TeXif{\if c#2}{d}{\TeXif{\if d#2}{d}{\TeXif{\if e#2}{f}{\TeXif{\if f#2}{f}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if a#1}{%
\TeXif{\if 0#2}{a}{\TeXif{\if 1#2}{b}{\TeXif{\if 2#2}{a}{\TeXif{\if 3#2}{b}{\TeXif{\if 4#2}{e}{\TeXif{\if 5#2}{f}{\TeXif{\if 6#2}{e}{\TeXif{\if 7#2}{f}{\TeXif{\if 8#2}{a}{\TeXif{\if 9#2}{b}{\TeXif{\if a#2}{a}{\TeXif{\if b#2}{b}{\TeXif{\if c#2}{e}{\TeXif{\if d#2}{f}{\TeXif{\if e#2}{e}{\TeXif{\if f#2}{f}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if b#1}{%
\TeXif{\if 0#2}{b}{\TeXif{\if 1#2}{b}{\TeXif{\if 2#2}{b}{\TeXif{\if 3#2}{b}{\TeXif{\if 4#2}{f}{\TeXif{\if 5#2}{f}{\TeXif{\if 6#2}{f}{\TeXif{\if 7#2}{f}{\TeXif{\if 8#2}{b}{\TeXif{\if 9#2}{b}{\TeXif{\if a#2}{b}{\TeXif{\if b#2}{b}{\TeXif{\if c#2}{f}{\TeXif{\if d#2}{f}{\TeXif{\if e#2}{f}{\TeXif{\if f#2}{f}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if c#1}{%
\TeXif{\if 0#2}{c}{\TeXif{\if 1#2}{d}{\TeXif{\if 2#2}{e}{\TeXif{\if 3#2}{f}{\TeXif{\if 4#2}{c}{\TeXif{\if 5#2}{d}{\TeXif{\if 6#2}{e}{\TeXif{\if 7#2}{f}{\TeXif{\if 8#2}{c}{\TeXif{\if 9#2}{d}{\TeXif{\if a#2}{e}{\TeXif{\if b#2}{f}{\TeXif{\if c#2}{c}{\TeXif{\if d#2}{d}{\TeXif{\if e#2}{e}{\TeXif{\if f#2}{f}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if d#1}{%
\TeXif{\if 0#2}{d}{\TeXif{\if 1#2}{d}{\TeXif{\if 2#2}{f}{\TeXif{\if 3#2}{f}{\TeXif{\if 4#2}{d}{\TeXif{\if 5#2}{d}{\TeXif{\if 6#2}{f}{\TeXif{\if 7#2}{f}{\TeXif{\if 8#2}{d}{\TeXif{\if 9#2}{d}{\TeXif{\if a#2}{f}{\TeXif{\if b#2}{f}{\TeXif{\if c#2}{d}{\TeXif{\if d#2}{d}{\TeXif{\if e#2}{f}{\TeXif{\if f#2}{f}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if e#1}{%
\TeXif{\if 0#2}{e}{\TeXif{\if 1#2}{f}{\TeXif{\if 2#2}{e}{\TeXif{\if 3#2}{f}{\TeXif{\if 4#2}{e}{\TeXif{\if 5#2}{f}{\TeXif{\if 6#2}{e}{\TeXif{\if 7#2}{f}{\TeXif{\if 8#2}{e}{\TeXif{\if 9#2}{f}{\TeXif{\if a#2}{e}{\TeXif{\if b#2}{f}{\TeXif{\if c#2}{e}{\TeXif{\if d#2}{f}{\TeXif{\if e#2}{e}{\TeXif{\if f#2}{f}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if f#1}{%
\TeXif{\if 0#2}{f}{\TeXif{\if 1#2}{f}{\TeXif{\if 2#2}{f}{\TeXif{\if 3#2}{f}{\TeXif{\if 4#2}{f}{\TeXif{\if 5#2}{f}{\TeXif{\if 6#2}{f}{\TeXif{\if 7#2}{f}{\TeXif{\if 8#2}{f}{\TeXif{\if 9#2}{f}{\TeXif{\if a#2}{f}{\TeXif{\if b#2}{f}{\TeXif{\if c#2}{f}{\TeXif{\if d#2}{f}{\TeXif{\if e#2}{f}{\TeXif{\if f#2}{f}{\Error}}}}}}}}}}}}}}}}}{\Error}}}}}}}}}}}}}}}}%
}
\def\SingleHexX@r#1#2{%
\TeXif{\if 0#1}{%
\TeXif{\if 0#2}{0}{\TeXif{\if 1#2}{1}{\TeXif{\if 2#2}{2}{\TeXif{\if 3#2}{3}{\TeXif{\if 4#2}{4}{\TeXif{\if 5#2}{5}{\TeXif{\if 6#2}{6}{\TeXif{\if 7#2}{7}{\TeXif{\if 8#2}{8}{\TeXif{\if 9#2}{9}{\TeXif{\if a#2}{a}{\TeXif{\if b#2}{b}{\TeXif{\if c#2}{c}{\TeXif{\if d#2}{d}{\TeXif{\if e#2}{e}{\TeXif{\if f#2}{f}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 1#1}{%
\TeXif{\if 0#2}{1}{\TeXif{\if 1#2}{0}{\TeXif{\if 2#2}{3}{\TeXif{\if 3#2}{2}{\TeXif{\if 4#2}{5}{\TeXif{\if 5#2}{4}{\TeXif{\if 6#2}{7}{\TeXif{\if 7#2}{6}{\TeXif{\if 8#2}{9}{\TeXif{\if 9#2}{8}{\TeXif{\if a#2}{b}{\TeXif{\if b#2}{a}{\TeXif{\if c#2}{d}{\TeXif{\if d#2}{c}{\TeXif{\if e#2}{f}{\TeXif{\if f#2}{e}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 2#1}{%
\TeXif{\if 0#2}{2}{\TeXif{\if 1#2}{3}{\TeXif{\if 2#2}{0}{\TeXif{\if 3#2}{1}{\TeXif{\if 4#2}{6}{\TeXif{\if 5#2}{7}{\TeXif{\if 6#2}{4}{\TeXif{\if 7#2}{5}{\TeXif{\if 8#2}{a}{\TeXif{\if 9#2}{b}{\TeXif{\if a#2}{8}{\TeXif{\if b#2}{9}{\TeXif{\if c#2}{e}{\TeXif{\if d#2}{f}{\TeXif{\if e#2}{c}{\TeXif{\if f#2}{d}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 3#1}{%
\TeXif{\if 0#2}{3}{\TeXif{\if 1#2}{2}{\TeXif{\if 2#2}{1}{\TeXif{\if 3#2}{0}{\TeXif{\if 4#2}{7}{\TeXif{\if 5#2}{6}{\TeXif{\if 6#2}{5}{\TeXif{\if 7#2}{4}{\TeXif{\if 8#2}{b}{\TeXif{\if 9#2}{a}{\TeXif{\if a#2}{9}{\TeXif{\if b#2}{8}{\TeXif{\if c#2}{f}{\TeXif{\if d#2}{e}{\TeXif{\if e#2}{d}{\TeXif{\if f#2}{c}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 4#1}{%
\TeXif{\if 0#2}{4}{\TeXif{\if 1#2}{5}{\TeXif{\if 2#2}{6}{\TeXif{\if 3#2}{7}{\TeXif{\if 4#2}{0}{\TeXif{\if 5#2}{1}{\TeXif{\if 6#2}{2}{\TeXif{\if 7#2}{3}{\TeXif{\if 8#2}{c}{\TeXif{\if 9#2}{d}{\TeXif{\if a#2}{e}{\TeXif{\if b#2}{f}{\TeXif{\if c#2}{8}{\TeXif{\if d#2}{9}{\TeXif{\if e#2}{a}{\TeXif{\if f#2}{b}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 5#1}{%
\TeXif{\if 0#2}{5}{\TeXif{\if 1#2}{4}{\TeXif{\if 2#2}{7}{\TeXif{\if 3#2}{6}{\TeXif{\if 4#2}{1}{\TeXif{\if 5#2}{0}{\TeXif{\if 6#2}{3}{\TeXif{\if 7#2}{2}{\TeXif{\if 8#2}{d}{\TeXif{\if 9#2}{c}{\TeXif{\if a#2}{f}{\TeXif{\if b#2}{e}{\TeXif{\if c#2}{9}{\TeXif{\if d#2}{8}{\TeXif{\if e#2}{b}{\TeXif{\if f#2}{a}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 6#1}{%
\TeXif{\if 0#2}{6}{\TeXif{\if 1#2}{7}{\TeXif{\if 2#2}{4}{\TeXif{\if 3#2}{5}{\TeXif{\if 4#2}{2}{\TeXif{\if 5#2}{3}{\TeXif{\if 6#2}{0}{\TeXif{\if 7#2}{1}{\TeXif{\if 8#2}{e}{\TeXif{\if 9#2}{f}{\TeXif{\if a#2}{c}{\TeXif{\if b#2}{d}{\TeXif{\if c#2}{a}{\TeXif{\if d#2}{b}{\TeXif{\if e#2}{8}{\TeXif{\if f#2}{9}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 7#1}{%
\TeXif{\if 0#2}{7}{\TeXif{\if 1#2}{6}{\TeXif{\if 2#2}{5}{\TeXif{\if 3#2}{4}{\TeXif{\if 4#2}{3}{\TeXif{\if 5#2}{2}{\TeXif{\if 6#2}{1}{\TeXif{\if 7#2}{0}{\TeXif{\if 8#2}{f}{\TeXif{\if 9#2}{e}{\TeXif{\if a#2}{d}{\TeXif{\if b#2}{c}{\TeXif{\if c#2}{b}{\TeXif{\if d#2}{a}{\TeXif{\if e#2}{9}{\TeXif{\if f#2}{8}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 8#1}{%
\TeXif{\if 0#2}{8}{\TeXif{\if 1#2}{9}{\TeXif{\if 2#2}{a}{\TeXif{\if 3#2}{b}{\TeXif{\if 4#2}{c}{\TeXif{\if 5#2}{d}{\TeXif{\if 6#2}{e}{\TeXif{\if 7#2}{f}{\TeXif{\if 8#2}{0}{\TeXif{\if 9#2}{1}{\TeXif{\if a#2}{2}{\TeXif{\if b#2}{3}{\TeXif{\if c#2}{4}{\TeXif{\if d#2}{5}{\TeXif{\if e#2}{6}{\TeXif{\if f#2}{7}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 9#1}{%
\TeXif{\if 0#2}{9}{\TeXif{\if 1#2}{8}{\TeXif{\if 2#2}{b}{\TeXif{\if 3#2}{a}{\TeXif{\if 4#2}{d}{\TeXif{\if 5#2}{c}{\TeXif{\if 6#2}{f}{\TeXif{\if 7#2}{e}{\TeXif{\if 8#2}{1}{\TeXif{\if 9#2}{0}{\TeXif{\if a#2}{3}{\TeXif{\if b#2}{2}{\TeXif{\if c#2}{5}{\TeXif{\if d#2}{4}{\TeXif{\if e#2}{7}{\TeXif{\if f#2}{6}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if a#1}{%
\TeXif{\if 0#2}{a}{\TeXif{\if 1#2}{b}{\TeXif{\if 2#2}{8}{\TeXif{\if 3#2}{9}{\TeXif{\if 4#2}{e}{\TeXif{\if 5#2}{f}{\TeXif{\if 6#2}{c}{\TeXif{\if 7#2}{d}{\TeXif{\if 8#2}{2}{\TeXif{\if 9#2}{3}{\TeXif{\if a#2}{0}{\TeXif{\if b#2}{1}{\TeXif{\if c#2}{6}{\TeXif{\if d#2}{7}{\TeXif{\if e#2}{4}{\TeXif{\if f#2}{5}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if b#1}{%
\TeXif{\if 0#2}{b}{\TeXif{\if 1#2}{a}{\TeXif{\if 2#2}{9}{\TeXif{\if 3#2}{8}{\TeXif{\if 4#2}{f}{\TeXif{\if 5#2}{e}{\TeXif{\if 6#2}{d}{\TeXif{\if 7#2}{c}{\TeXif{\if 8#2}{3}{\TeXif{\if 9#2}{2}{\TeXif{\if a#2}{1}{\TeXif{\if b#2}{0}{\TeXif{\if c#2}{7}{\TeXif{\if d#2}{6}{\TeXif{\if e#2}{5}{\TeXif{\if f#2}{4}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if c#1}{%
\TeXif{\if 0#2}{c}{\TeXif{\if 1#2}{d}{\TeXif{\if 2#2}{e}{\TeXif{\if 3#2}{f}{\TeXif{\if 4#2}{8}{\TeXif{\if 5#2}{9}{\TeXif{\if 6#2}{a}{\TeXif{\if 7#2}{b}{\TeXif{\if 8#2}{4}{\TeXif{\if 9#2}{5}{\TeXif{\if a#2}{6}{\TeXif{\if b#2}{7}{\TeXif{\if c#2}{0}{\TeXif{\if d#2}{1}{\TeXif{\if e#2}{2}{\TeXif{\if f#2}{3}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if d#1}{%
\TeXif{\if 0#2}{d}{\TeXif{\if 1#2}{c}{\TeXif{\if 2#2}{f}{\TeXif{\if 3#2}{e}{\TeXif{\if 4#2}{9}{\TeXif{\if 5#2}{8}{\TeXif{\if 6#2}{b}{\TeXif{\if 7#2}{a}{\TeXif{\if 8#2}{5}{\TeXif{\if 9#2}{4}{\TeXif{\if a#2}{7}{\TeXif{\if b#2}{6}{\TeXif{\if c#2}{1}{\TeXif{\if d#2}{0}{\TeXif{\if e#2}{3}{\TeXif{\if f#2}{2}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if e#1}{%
\TeXif{\if 0#2}{e}{\TeXif{\if 1#2}{f}{\TeXif{\if 2#2}{c}{\TeXif{\if 3#2}{d}{\TeXif{\if 4#2}{a}{\TeXif{\if 5#2}{b}{\TeXif{\if 6#2}{8}{\TeXif{\if 7#2}{9}{\TeXif{\if 8#2}{6}{\TeXif{\if 9#2}{7}{\TeXif{\if a#2}{4}{\TeXif{\if b#2}{5}{\TeXif{\if c#2}{2}{\TeXif{\if d#2}{3}{\TeXif{\if e#2}{0}{\TeXif{\if f#2}{1}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if f#1}{%
\TeXif{\if 0#2}{f}{\TeXif{\if 1#2}{e}{\TeXif{\if 2#2}{d}{\TeXif{\if 3#2}{c}{\TeXif{\if 4#2}{b}{\TeXif{\if 5#2}{a}{\TeXif{\if 6#2}{9}{\TeXif{\if 7#2}{8}{\TeXif{\if 8#2}{7}{\TeXif{\if 9#2}{6}{\TeXif{\if a#2}{5}{\TeXif{\if b#2}{4}{\TeXif{\if c#2}{3}{\TeXif{\if d#2}{2}{\TeXif{\if e#2}{1}{\TeXif{\if f#2}{0}{\Error}}}}}}}}}}}}}}}}}{\Error}}}}}}}}}}}}}}}}%
}

\def\SingleHex@nd#1#2{
\TeXif{\if 0#1}{%
\TeXif{\if 0#2}{0}{\TeXif{\if 1#2}{0}{\TeXif{\if 2#2}{0}{\TeXif{\if 3#2}{0}{\TeXif{\if 4#2}{0}{\TeXif{\if 5#2}{0}{\TeXif{\if 6#2}{0}{\TeXif{\if 7#2}{0}{\TeXif{\if 8#2}{0}{\TeXif{\if 9#2}{0}{\TeXif{\if a#2}{0}{\TeXif{\if b#2}{0}{\TeXif{\if c#2}{0}{\TeXif{\if d#2}{0}{\TeXif{\if e#2}{0}{\TeXif{\if f#2}{0}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 1#1}{%
\TeXif{\if 0#2}{0}{\TeXif{\if 1#2}{1}{\TeXif{\if 2#2}{0}{\TeXif{\if 3#2}{1}{\TeXif{\if 4#2}{0}{\TeXif{\if 5#2}{1}{\TeXif{\if 6#2}{0}{\TeXif{\if 7#2}{1}{\TeXif{\if 8#2}{0}{\TeXif{\if 9#2}{1}{\TeXif{\if a#2}{0}{\TeXif{\if b#2}{1}{\TeXif{\if c#2}{0}{\TeXif{\if d#2}{1}{\TeXif{\if e#2}{0}{\TeXif{\if f#2}{1}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 2#1}{%
\TeXif{\if 0#2}{0}{\TeXif{\if 1#2}{0}{\TeXif{\if 2#2}{2}{\TeXif{\if 3#2}{2}{\TeXif{\if 4#2}{0}{\TeXif{\if 5#2}{0}{\TeXif{\if 6#2}{2}{\TeXif{\if 7#2}{2}{\TeXif{\if 8#2}{0}{\TeXif{\if 9#2}{0}{\TeXif{\if a#2}{2}{\TeXif{\if b#2}{2}{\TeXif{\if c#2}{0}{\TeXif{\if d#2}{0}{\TeXif{\if e#2}{2}{\TeXif{\if f#2}{2}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 3#1}{%
\TeXif{\if 0#2}{0}{\TeXif{\if 1#2}{1}{\TeXif{\if 2#2}{2}{\TeXif{\if 3#2}{3}{\TeXif{\if 4#2}{0}{\TeXif{\if 5#2}{1}{\TeXif{\if 6#2}{2}{\TeXif{\if 7#2}{3}{\TeXif{\if 8#2}{0}{\TeXif{\if 9#2}{1}{\TeXif{\if a#2}{2}{\TeXif{\if b#2}{3}{\TeXif{\if c#2}{0}{\TeXif{\if d#2}{1}{\TeXif{\if e#2}{2}{\TeXif{\if f#2}{3}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 4#1}{%
\TeXif{\if 0#2}{0}{\TeXif{\if 1#2}{0}{\TeXif{\if 2#2}{0}{\TeXif{\if 3#2}{0}{\TeXif{\if 4#2}{4}{\TeXif{\if 5#2}{4}{\TeXif{\if 6#2}{4}{\TeXif{\if 7#2}{4}{\TeXif{\if 8#2}{0}{\TeXif{\if 9#2}{0}{\TeXif{\if a#2}{0}{\TeXif{\if b#2}{0}{\TeXif{\if c#2}{4}{\TeXif{\if d#2}{4}{\TeXif{\if e#2}{4}{\TeXif{\if f#2}{4}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 5#1}{%
\TeXif{\if 0#2}{0}{\TeXif{\if 1#2}{1}{\TeXif{\if 2#2}{0}{\TeXif{\if 3#2}{1}{\TeXif{\if 4#2}{4}{\TeXif{\if 5#2}{5}{\TeXif{\if 6#2}{4}{\TeXif{\if 7#2}{5}{\TeXif{\if 8#2}{0}{\TeXif{\if 9#2}{1}{\TeXif{\if a#2}{0}{\TeXif{\if b#2}{1}{\TeXif{\if c#2}{4}{\TeXif{\if d#2}{5}{\TeXif{\if e#2}{4}{\TeXif{\if f#2}{5}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 6#1}{%
\TeXif{\if 0#2}{0}{\TeXif{\if 1#2}{0}{\TeXif{\if 2#2}{2}{\TeXif{\if 3#2}{2}{\TeXif{\if 4#2}{4}{\TeXif{\if 5#2}{4}{\TeXif{\if 6#2}{6}{\TeXif{\if 7#2}{6}{\TeXif{\if 8#2}{0}{\TeXif{\if 9#2}{0}{\TeXif{\if a#2}{2}{\TeXif{\if b#2}{2}{\TeXif{\if c#2}{4}{\TeXif{\if d#2}{4}{\TeXif{\if e#2}{6}{\TeXif{\if f#2}{6}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 7#1}{%
\TeXif{\if 0#2}{0}{\TeXif{\if 1#2}{1}{\TeXif{\if 2#2}{2}{\TeXif{\if 3#2}{3}{\TeXif{\if 4#2}{4}{\TeXif{\if 5#2}{5}{\TeXif{\if 6#2}{6}{\TeXif{\if 7#2}{7}{\TeXif{\if 8#2}{0}{\TeXif{\if 9#2}{1}{\TeXif{\if a#2}{2}{\TeXif{\if b#2}{3}{\TeXif{\if c#2}{4}{\TeXif{\if d#2}{5}{\TeXif{\if e#2}{6}{\TeXif{\if f#2}{7}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 8#1}{%
\TeXif{\if 0#2}{0}{\TeXif{\if 1#2}{0}{\TeXif{\if 2#2}{0}{\TeXif{\if 3#2}{0}{\TeXif{\if 4#2}{0}{\TeXif{\if 5#2}{0}{\TeXif{\if 6#2}{0}{\TeXif{\if 7#2}{0}{\TeXif{\if 8#2}{8}{\TeXif{\if 9#2}{8}{\TeXif{\if a#2}{8}{\TeXif{\if b#2}{8}{\TeXif{\if c#2}{8}{\TeXif{\if d#2}{8}{\TeXif{\if e#2}{8}{\TeXif{\if f#2}{8}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if 9#1}{%
\TeXif{\if 0#2}{0}{\TeXif{\if 1#2}{1}{\TeXif{\if 2#2}{0}{\TeXif{\if 3#2}{1}{\TeXif{\if 4#2}{0}{\TeXif{\if 5#2}{1}{\TeXif{\if 6#2}{0}{\TeXif{\if 7#2}{1}{\TeXif{\if 8#2}{8}{\TeXif{\if 9#2}{9}{\TeXif{\if a#2}{8}{\TeXif{\if b#2}{9}{\TeXif{\if c#2}{8}{\TeXif{\if d#2}{9}{\TeXif{\if e#2}{8}{\TeXif{\if f#2}{9}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if a#1}{%
\TeXif{\if 0#2}{0}{\TeXif{\if 1#2}{0}{\TeXif{\if 2#2}{2}{\TeXif{\if 3#2}{2}{\TeXif{\if 4#2}{0}{\TeXif{\if 5#2}{0}{\TeXif{\if 6#2}{2}{\TeXif{\if 7#2}{2}{\TeXif{\if 8#2}{8}{\TeXif{\if 9#2}{8}{\TeXif{\if a#2}{a}{\TeXif{\if b#2}{a}{\TeXif{\if c#2}{8}{\TeXif{\if d#2}{8}{\TeXif{\if e#2}{a}{\TeXif{\if f#2}{a}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if b#1}{%
\TeXif{\if 0#2}{0}{\TeXif{\if 1#2}{1}{\TeXif{\if 2#2}{2}{\TeXif{\if 3#2}{3}{\TeXif{\if 4#2}{0}{\TeXif{\if 5#2}{1}{\TeXif{\if 6#2}{2}{\TeXif{\if 7#2}{3}{\TeXif{\if 8#2}{8}{\TeXif{\if 9#2}{9}{\TeXif{\if a#2}{a}{\TeXif{\if b#2}{b}{\TeXif{\if c#2}{8}{\TeXif{\if d#2}{9}{\TeXif{\if e#2}{a}{\TeXif{\if f#2}{b}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if c#1}{%
\TeXif{\if 0#2}{0}{\TeXif{\if 1#2}{0}{\TeXif{\if 2#2}{0}{\TeXif{\if 3#2}{0}{\TeXif{\if 4#2}{4}{\TeXif{\if 5#2}{4}{\TeXif{\if 6#2}{4}{\TeXif{\if 7#2}{4}{\TeXif{\if 8#2}{8}{\TeXif{\if 9#2}{8}{\TeXif{\if a#2}{8}{\TeXif{\if b#2}{8}{\TeXif{\if c#2}{c}{\TeXif{\if d#2}{c}{\TeXif{\if e#2}{c}{\TeXif{\if f#2}{c}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if d#1}{%
\TeXif{\if 0#2}{0}{\TeXif{\if 1#2}{1}{\TeXif{\if 2#2}{0}{\TeXif{\if 3#2}{1}{\TeXif{\if 4#2}{4}{\TeXif{\if 5#2}{5}{\TeXif{\if 6#2}{4}{\TeXif{\if 7#2}{5}{\TeXif{\if 8#2}{8}{\TeXif{\if 9#2}{9}{\TeXif{\if a#2}{8}{\TeXif{\if b#2}{9}{\TeXif{\if c#2}{c}{\TeXif{\if d#2}{d}{\TeXif{\if e#2}{c}{\TeXif{\if f#2}{d}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if e#1}{%
\TeXif{\if 0#2}{0}{\TeXif{\if 1#2}{0}{\TeXif{\if 2#2}{2}{\TeXif{\if 3#2}{2}{\TeXif{\if 4#2}{4}{\TeXif{\if 5#2}{4}{\TeXif{\if 6#2}{6}{\TeXif{\if 7#2}{6}{\TeXif{\if 8#2}{8}{\TeXif{\if 9#2}{8}{\TeXif{\if a#2}{a}{\TeXif{\if b#2}{a}{\TeXif{\if c#2}{c}{\TeXif{\if d#2}{c}{\TeXif{\if e#2}{e}{\TeXif{\if f#2}{e}{\Error}}}}}}}}}}}}}}}}}{\TeXif{\if f#1}{%
\TeXif{\if 0#2}{0}{\TeXif{\if 1#2}{1}{\TeXif{\if 2#2}{2}{\TeXif{\if 3#2}{3}{\TeXif{\if 4#2}{4}{\TeXif{\if 5#2}{5}{\TeXif{\if 6#2}{6}{\TeXif{\if 7#2}{7}{\TeXif{\if 8#2}{8}{\TeXif{\if 9#2}{9}{\TeXif{\if a#2}{a}{\TeXif{\if b#2}{b}{\TeXif{\if c#2}{c}{\TeXif{\if d#2}{d}{\TeXif{\if e#2}{e}{\TeXif{\if f#2}{f}{\Error}}}}}}}}}}}}}}}}}{\Error}}}}}}}}}}}}}}}}%
}

\def\HexGetFirstBit#1{%
	
}

\def\HexRightShiftResolve#1#2{
	{\Cat{\HexRightShiftResolveHelper{#1}{\Second{#2}}}{\First{#2}}}{\HexGetFirstBit{#1}}
}

\def\HexRightShiftHelper#1{
	\HexRightShiftResolve{\Head{#1}}{\HexRightShiftHelper{#2}}
}

\def\HexRightShiftOne#1{%
	\First{\HexRightShiftHelper{#1}}%
}

\def\HexXor{\Merge\SingleHexX@r\Error}
\def\HexAnd{\Merge\SingleHex@nd\Error}
\def\HexOr{\Merge\SingleHex@r\Error}
\def\HexNot{\Map\SingleHexN@t}

\message{\Unlistize{\HexNot{\Tokenlistize{f0}}}}

\def\Chartohexlist#1%
	{\TeXif{\if0#1}{\Tokenlistize{03}}%
	{\TeXif{\if1#1}{\Tokenlistize{13}}%
	{\TeXif{\if2#1}{\Tokenlistize{23}}%
	{\TeXif{\if3#1}{\Tokenlistize{33}}%
	{\TeXif{\if4#1}{\Tokenlistize{43}}%
	{\TeXif{\if5#1}{\Tokenlistize{53}}%
	{\TeXif{\if6#1}{\Tokenlistize{63}}%
	{\TeXif{\if7#1}{\Tokenlistize{73}}%
	{\TeXif{\if8#1}{\Tokenlistize{83}}%
	{\TeXif{\if9#1}{\Tokenlistize{93}}%
	\Error}}}}}}}}}}
\def\Twocharstohexlist#1#2{\Cat{\Chartohexlist{#2}}{\Chartohexlist{#1}}}
\def\Threecharstohexlist#1#2#3{\Cat{\Chartohexlist{#3}}%
                                   {\Twocharstohexlist{#1}{#2}}}
\def\Fourcharstohexlist#1#2#3#4{\Cat{\Chartohexlist{#4}}%
                                    {\Threecharstohexlist{#1}{#2}{#3}}}
\def\Maybefourcharstohexlist#1#2#3#4%
	{\TeXif{\ifx\relax#2}{\Chartohexlist{#1}}%
	{\TeXif{\ifx\relax#3}{\Twocharstohexlist{#1}{#2}}%
	{\TeXif{\ifx\relax#4}{\Threecharstohexlist{#1}{#2}{#3}}%
	{\Fourcharstohexlist{#1}{#2}{#3}{#4}}}}}
\message{\Unlistize{\Maybefourcharstohexlist123\relax}}

%% BROKEN
%\def\Stringtohexarray#1{\Stringtohexarray@#1\relax\relax\relax\relax]}
%\def\Stringtohexarray@#1#2#3#4#5]%
%	{\TeXif{\ifx\relax#5}%
%		{\Singleton{\Maybefourcharstohexlist{#1}{#2}{#3}{#4}}}%
%		{\Cons{\Maybefourcharstohexlist{#1}{#2}{#3}{#4}}%
%		      {\Stringtohexarray@#5}}}
%\def\Unarrayize#1{[#1\Unarrayize@{}]}
%\def\Unarrayize@#1{0x\Untokenlistize{#1}\Foldr\Arraycommaize{}}
%\def\Arraycommaize#1#2{, 0x\Untokenlistize{#1}#2}
%\message{\Unarrayize{\Stringtohexarray{1234567}}}

\bye
