#!/usr/bin/perl

use Digest::SHA ("sha256_hex");

my $salt = getSalt();

print <<TOP;
\\def\\shalookup#1{%
\\ifcase#1%
TOP
for (my $i = 0; $i <= 500; ++$i) {
	if ($i != 0) {
		print "\t\\or  ";
	} else {
		print "\t     ";
	}
	printf("%s%%\n", hash($i, $salt));
}
print <<BOTTOM;
\t\\else\\errmessage{tl;dr}%
\\fi%
}
BOTTOM


sub hash {
	my $num = shift;
	my $salt = shift;
	my $digest = sha256_hex($num . $salt);
	return "0x".$digest;
}

sub getSalt {
	open SALT, "salt.txt" or die $!;
	my $salt = <SALT>;
	close SALT;
	return $salt;
}
